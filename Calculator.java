import java.util.Random;

public class Calculator{
	public static int sumCalculator(int sum1, int sum2){
		return sum1 + sum2;
	}
	public static double square(int squareIn){
		//uses Math package
		return Math.sqrt(squareIn);
	}
	public static int randomizer(){
		Random rando = new Random();
		return rando.nextInt();
	}
	public static int divisor(int dent, int sor){
		//if the divisor is 0, then just return 0 and throw an error
		//since division by 0 is impossible
		if(sor == 0){
			System.out.println("CANNOT DIVIDE BY ZERO");
			return 0;
		}
		else
			return dent / sor;
	}
}