import java.util.Scanner;

public class Driver{
	public static void main(String[] args){
		int x;
		int y;
		int z;
		
		Scanner inputer = new Scanner (System.in);
		
		System.out.println("Hello user!");
		
		//calculates sum
		System.out.println("Input 2 numbers to add together" + "\r\n" + "Input the first number");
		
			x = inputer.nextInt();
		
		System.out.println("Input the second number");
		
			y = inputer.nextInt();
		
		System.out.println("Here is your results: " + Calculator.sumCalculator(x,y));
		
		//calculates quotient
		
		System.out.println("Input 2 numbers to add together" + "\r\n" + "Input the first number (dividend)");
		
			x = inputer.nextInt();
		
		System.out.println("Input the second number (divisor)");
		
			y = inputer.nextInt();
		
		System.out.println("Here is your results: " + Calculator.divisor(x,y));
		
		//calculates square roots
		System.out.println("Input a single number to obtain the square root: ");
		
			z = inputer.nextInt();
		
		System.out.println("Here is your results: " + Calculator.square(z));
		
		//randomizer
		
		System.out.println("Here's a random number: " + Calculator.randomizer());
		
		//closes scanner after operations
		inputer.close();
		
	} 
}